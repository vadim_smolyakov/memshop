package com.example.home.memshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MemshopApplication {

	public static void main(String[] args) {
		SpringApplication.run(MemshopApplication.class, args);
	}
}
