package com.example.home.memshop.controller;

import com.example.home.memshop.entity.Mem;
import com.example.home.memshop.entity.MemType;
import com.example.home.memshop.entity.User;
import com.example.home.memshop.exception.MoneyException;
import com.example.home.memshop.service.MemService;
import com.example.home.memshop.service.MemTypeService;
import com.example.home.memshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@Controller
public class MainController {

    @Autowired
    private UserService userService;

    @Autowired
    private MemService memService;

    @Autowired
    private MemTypeService memTypeService;

    @GetMapping("/memshop")
    public String index(Model model) {
        List<MemType> memTypeList = memTypeService.findAll();
        HashMap<MemType, List<Mem>> map = new HashMap<>();

        memTypeList.forEach(memType -> map.put(memType, memType.getMems()));

        model.addAttribute("map", map);

        return "index";
    }

    @GetMapping("/mem/{id}")
    public String mem(@PathVariable("id") Long id,Model model) {
        Mem mem = memService.findById(id);
        List<MemType> memTypes = memTypeService.findAll();

        model.addAttribute("mem", mem);
        model.addAttribute("typeList", memTypes);

        return "mem";
    }

    @GetMapping("/memes/{memType}")
    public String memesByType(@PathVariable("memType") String memType, Model model) {
        MemType type = memTypeService.findByName(memType);
        List<MemType> memTypes = memTypeService.findAll();

        model.addAttribute("mems", type.getMems());
        model.addAttribute("currentType", memType);
        model.addAttribute("typesList", memTypes);

        return "memesByType";
    }

    @PostMapping("/buy/{id}")
    public String buy(@ModelAttribute("username") String username, @PathVariable("id") Long id, Model model) throws MoneyException {
        userService.buyMem(username, id);

        return "redirect:/profile/" + username;
    }
}
