package com.example.home.memshop.dto;

import com.example.home.memshop.entity.MemType;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class MemCreatingDto {
    private String name;
    private String description;
    private BigDecimal price;
    private Integer quantity;
    private MemType memType;

    public MemCreatingDto(String name, String description, BigDecimal price, Integer quantity, MemType memType) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.quantity = quantity;
        this.memType = memType;
    }
}
