package com.example.home.memshop.dto;

import lombok.Data;

@Data
public class UserRegistrationDto {
    private String username;
    private String password;

    public UserRegistrationDto(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
