package com.example.home.memshop.entity;

import com.sun.org.apache.xpath.internal.functions.WrongNumberArgsException;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "mems")
@Data
public class Mem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "image_path")
    private String imagePath;

    @ManyToOne
    @JoinColumn(name = "mem_type_id")
    private MemType memType;

    @ManyToMany(mappedBy = "boughtMems")
    private List<User> customers;

    public Mem() {
    }

    public Mem(String name, String description, Integer quantity, BigDecimal price, MemType memType) {
        this.name = name;
        this.description = description;
        this.quantity = quantity;
        this.price = price;
        this.memType = memType;
    }

    @Override
    public String toString() {
        return "Mem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                ", imagePath='" + imagePath + '\'' +
                ", memType=" + memType +
                ", customers=" + customers +
                '}';
    }

    public void changePrice(BigDecimal newPrice) throws WrongNumberArgsException {
        if (isPositive(newPrice)) {
            this.price = newPrice;
        }
        throw new WrongNumberArgsException("wrong new price");
    }

    private boolean isPositive(BigDecimal newPrice) {
        return newPrice.compareTo(new BigDecimal("0")) > 0;
    }

    public void decreaseQuantity(int number) {
        this.quantity = this.quantity - number;
    }
}
