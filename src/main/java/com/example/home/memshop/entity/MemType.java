package com.example.home.memshop.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "mem_types")
@Data
public class MemType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "memType")
    private List<Mem> mems;

    public MemType() {
    }

    public MemType(Long id, String name, List<Mem> mems) {
        this.id = id;
        this.name = name;
        this.mems = mems;
    }
}
