package com.example.home.memshop.entity;

import com.example.home.memshop.exception.MoneyException;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users")
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

//    @Transient
//    private String confirmPassword;

    @Column(name = "account_balance")
    private BigDecimal accountBalance = new BigDecimal("0");

    @ManyToMany
    @JoinTable(name = "user_mems",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "mem_id"))
    private List<Mem> boughtMems = new ArrayList<>();

    public User() {

    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.accountBalance = new BigDecimal("0");
        this.boughtMems = new ArrayList<>();
    }

    public User(Long id, String username, String password, BigDecimal accountBalance, List<Mem> boughtMems) {
        this.id = id;
        this.username = username;
        this.password = password;
//        this.confirmPassword = confirmPassword;
        this.accountBalance = accountBalance;
        this.boughtMems = boughtMems;
    }

    public void buy(Mem mem) throws MoneyException {
        if (isEnoughMoney(mem.getPrice())) {
            this.chargeOff(mem.getPrice());

            boughtMems.add(mem);
            mem.decreaseQuantity(1);
        } else {
            throw new MoneyException("not enough money");
        }
    }

    private boolean isEnoughMoney(BigDecimal price) {
        return this.accountBalance.compareTo(price) >= 0;
    }

    private void chargeOff(BigDecimal money) {
        this.accountBalance = this.accountBalance.subtract(money);
    }

    public void putMoney(BigDecimal money) {
        this.accountBalance = this.accountBalance.add(money);
    }

    public int getMemesQuantity() {
        return boughtMems.size();
    }
}
