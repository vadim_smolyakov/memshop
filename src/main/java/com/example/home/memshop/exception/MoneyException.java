package com.example.home.memshop.exception;

public class MoneyException extends Exception {
    private String name;

    public MoneyException(String name) {
        this.name = name;
    }

    public MoneyException(String message, String name) {
        super(message);
        this.name = name;
    }

    public MoneyException(String message, Throwable cause, String name) {
        super(message, cause);
        this.name = name;
    }

    public MoneyException(Throwable cause, String name) {
        super(cause);
        this.name = name;
    }

    public MoneyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String name) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.name = name;
    }
}
