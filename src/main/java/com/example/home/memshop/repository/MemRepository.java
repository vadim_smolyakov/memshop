package com.example.home.memshop.repository;

import com.example.home.memshop.entity.Mem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MemRepository extends JpaRepository<Mem, Long> {
    Mem findByName(String name);
}
