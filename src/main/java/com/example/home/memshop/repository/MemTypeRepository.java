package com.example.home.memshop.repository;

import com.example.home.memshop.entity.MemType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNullApi;

import java.util.List;

public interface MemTypeRepository extends JpaRepository<MemType, Long> {
    MemType findByName(String name);

    List<MemType> findAll();
}
