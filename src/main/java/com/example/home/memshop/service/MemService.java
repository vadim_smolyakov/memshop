package com.example.home.memshop.service;

import com.example.home.memshop.dto.MemCreatingDto;
import com.example.home.memshop.entity.Mem;

public interface MemService {
    void save(MemCreatingDto memCreatingDto);

    Mem findByName(String name);

    Mem findById(Long id);
}
