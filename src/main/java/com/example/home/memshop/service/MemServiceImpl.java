package com.example.home.memshop.service;

import com.example.home.memshop.dto.MemCreatingDto;
import com.example.home.memshop.entity.Mem;
import com.example.home.memshop.repository.MemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MemServiceImpl implements MemService {

    @Autowired
    private MemRepository memRepository;

    @Override
    public void save(MemCreatingDto memCreatingDto) {
        Mem mem = new Mem(memCreatingDto.getName(), memCreatingDto.getDescription(),
                memCreatingDto.getQuantity(), memCreatingDto.getPrice(), memCreatingDto.getMemType());

        memRepository.save(mem);
    }

    @Override
    public Mem findByName(String name) {
        return memRepository.findByName(name);
    }

    @Override
    public Mem findById(Long id) {
        return memRepository.getOne(id);
    }
}
