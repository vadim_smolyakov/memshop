package com.example.home.memshop.service;

import com.example.home.memshop.entity.MemType;

import java.util.List;

public interface MemTypeService {
    MemType findByName(String name);

    void save(MemType memType);

    List<MemType> findAll();
}
