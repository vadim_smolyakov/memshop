package com.example.home.memshop.service;

import com.example.home.memshop.entity.MemType;
import com.example.home.memshop.repository.MemTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MemTypeServiceImpl implements MemTypeService{
    @Autowired
    private MemTypeRepository memTypeRepository;

    @Override
    public MemType findByName(String name) {
        if (name == null) {

        }
        return memTypeRepository.findByName(name);
    }

    @Override
    public void save(MemType memType) {
        memTypeRepository.save(memType);
    }

    @Override
    public List<MemType> findAll() {
        return memTypeRepository.findAll();
    }
}
