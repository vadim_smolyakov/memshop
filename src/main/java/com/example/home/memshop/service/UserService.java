package com.example.home.memshop.service;

import com.example.home.memshop.dto.UserRegistrationDto;
import com.example.home.memshop.entity.User;
import com.example.home.memshop.exception.MoneyException;

import java.math.BigDecimal;

public interface UserService {
    void save(UserRegistrationDto user);

    User findByUsername(String username);

    void buyMem(String username, Long id) throws MoneyException;

    void putMoneyOnAccount(String username, BigDecimal money) throws MoneyException;

    void update(User user);
}
