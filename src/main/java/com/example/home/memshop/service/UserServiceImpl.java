package com.example.home.memshop.service;

import com.example.home.memshop.dto.UserRegistrationDto;
import com.example.home.memshop.entity.Mem;
import com.example.home.memshop.entity.User;
import com.example.home.memshop.exception.MoneyException;
import com.example.home.memshop.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MemService memService;

    @Override
    public void save(UserRegistrationDto userRegistrationDto) {
        User user = new User(userRegistrationDto.getUsername(), userRegistrationDto.getPassword());

//        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));

        userRepository.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public void buyMem(String username, Long id) throws MoneyException {
        User user = findByUsername(username);
        Mem mem = memService.findById(id);

        if (mem.getQuantity() > 0) {
            user.buy(mem);
        }

        update(user);
    }

    @Override
    public void putMoneyOnAccount(String username, BigDecimal money) throws MoneyException {
        User user = this.userRepository.findByUsername(username);

        if (money.compareTo(new BigDecimal("0")) <= 0) {
            throw new MoneyException(money.toPlainString() + " sum is incorrect");
        }

        user.putMoney(money);
        update(user);
    }

    @Override
    public void update(User user) {
        this.userRepository.save(user);
    }
}
